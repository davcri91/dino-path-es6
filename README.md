# Dino Path

![Game screenshot](https://imgur.com/64OwE09.png)

## Story
    Dino is a little Italian Dinosaur. As all Italians, he eats a lot: help him gather food and avoid dangers.


## Why 
*Dino Path* is a really small prototype game that I built for a game programmer interview. 
The company develops games for kids, so I decided to keep the UI and the goal of the game as simple and intuitive as possible.

Tech-wise I had to use Phaser which, it's based on an old JavaScript 
version that does not support classes and other OOP features. 
For this reason I based my project on the 
[ES6 Phaser Boilerplate](https://github.com/lean/phaser-es6-webpack/tree/master).

Unfortunately the interview never took place, so I decided to open source this prototype. Maybe it will be useful for someone who is starting with Phaser. 

Don't have high expectations for this game because:
1. it was my first time using Phaser
2. I had little free time to work on this (I was working when I started coding this game)

## Licensing

The code is released under MIT License so you can reuse it however you want but
**don't reuse the assets** because they come from multiple sources and I'm not sure about licensing. 

# Run the game
## 1. Clone this repo

```git clone https://gitlab.com/davcri91/dino-path-es6.git```

## 2. Install node.js and npm

https://nodejs.org/en/


## 3. Install dependencies 

```npm install``` 

or if you chose yarn, just run ```yarn```

## 4. Run the development server:

```npm run dev```