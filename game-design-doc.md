# Dino Path - mini game design document


In **Dino Path** you will help our friendly Dino in his way back home.
He had to adventure in the forest to take some good food and now he needs
to bring it back home.

Unfortunately some random monster will get in the way! Help Dino by giving him
directions and then assist him along the path by blocking enememies or use
objects to assist him in different creative ways!

## Design Changelog
Current version: 18.09.1 (calVer: YY.0M.MICRO)


-
- 18.09.1: initial version


# Characters

## Dino [Playable character]

**Dino** is a dinosaur, of Italian origins, and as most of Italian people he eats a lot. 
Curiosity: his name *Dino* is pronounced "Deeno" (like a common Italian person name) 
and not dino like in "dinosaur".

# Enemies

Rule R1 applies to every enemy.

## E1 - Defender
*Defender* moves every turn by one tile in the direction that he is facing.

Suppose that he starts in this way:

```
[ ] [ ] [ ]    [ ]  
[ ] [ ] [ ]    [ ]  
[ ] [ ] [D-up] [ ]  
[ ] [ ] [ ]    [ ]  
```

Note: D-up means *"Defender, facing upwards"*

The next turn he will move in the upper tile and he will change direction:

```
[ ] [ ] [ ]    [ ]  
[ ] [ ] [D-down] [ ]  
[ ] [ ] [ ]    [ ]  
[ ] [ ] [ ]    [ ]  
```

So in the third turn he will come back in the initial position:

```
[ ] [ ] [ ]    [ ]  
[ ] [ ] [ ]    [ ]  
[ ] [ ] [D-up] [ ]  
[ ] [ ] [ ]    [ ] 
```

## E2 - Bomber 
The *Bomber* puts a bomb in the tile in front of him every 3 turns.
The *Bomber* takes a step back in the successive turn.
The bomb explodes after 1 turn. The explosion propagates in the adjacent tiles.

Turn 1:
```
[ ] [ ] [ ] [ ]
[ ] [ ] [ ] [ ]
[ ] [ ] [B-up] [ ]
[ ] [ ] [ ] [ ]
```

Turn 2:
```
[ ] [ ] [ ] [ ]
[ ] [ ] [ ] [ ]
[ ] [ ] [B-up-takes-bomb] [ ]
[ ] [ ] [ ] [ ]
```

Turn 3:
```
[ ] [ ] [ ] [ ]
[ ] [ ] [bomb] [ ]
[ ] [ ] [B-up] [ ]
[ ] [ ] [ ] [ ]
```

Turn 4:
```
[ ] [ ] [x]    [ ]
[ ] [x] [bomb] [x]
[ ] [ ] [x]    [ ]
[ ] [ ] [B-down] [ ]
```

## E3 - 

...


# Mechanics
## 1. Space
The game is set in a discrete grid composed by 32x18 (16:9 aspect ratio) 
orthogonal tiles.


## 2. Objects
Objects are placed in the map and they can occupy one or more tiles.

Every level is randomly generated. This means that objects are placed in a 
different way for each level.

### Obj1 - Fence
1 Tile

### 0bj2 - Bomb 
1 Tile

## 3. Actions
The following is a list of operative actions possible in the game.

### A1 - Select path

### A2 - Play action card 


## 4. Rules

### R0 - Turns
Every entity in the game moves only a the beginning of a new turn.

### R1 - Damage on the same tile
If in the same turn Dino and an enemy move in the same tile, Dino lose 1 HP.

## 5. Skills
## 6. Probabilty
