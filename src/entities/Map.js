import config from "../config";
import { getRandomInt } from "../utils";

class Map {
    constructor({ game, group, ui }) {
        this.tileSize = 16
        this.scaledTileSize = this.tileSize * config.textureScaleFactor

        // store objects for later reference
        this.game = game
        this.ui = ui

        // create an empty tilemap
        this.tileMap = game.add.tilemap()
        // set the tileset image with 16x16px tiles
        this.tileMap.addTilesetImage('tuxemon-tileset', null, this.tileSize, this.tileSize)

        // default number of tiles for the map
        this.xTiles = 14
        this.yTiles = 10

        // create a TileLayer and add it to the main group
        this.level1 = this.tileMap.create('level1', this.xTiles, this.yTiles, this.tileSize, this.tileSize)
        group.addChildAt(this.level1, 0) // this ensures the map is drawn behind everything else
        this.tileMap.fill(1, 0, 0, this.xTiles, this.yTiles)
        this.level1.setScale(4)

        // stores the tile objects that has been selected by the user by clicking on them
        this.path = []
        // stores the graphic elements (in world coordinates) for the selected path (the tiles clicked when selecting the path)
        this.pathGraphics = []
        // stores the highlighted tiles for visual feedback for the next path (world coordinates)
        this.newPathGraphics = []
        this.displayPathHighlight = true

        // tile marker (world coordinates)
        this.marker = game.add.graphics();
        this.marker.lineStyle(4, 0x000000, 0.6); // width, col, alpha
        this.marker.drawRect(0, 0, this.scaledTileSize, this.scaledTileSize);
        
        this.flag = this.createFlag()
        group.addChildAt(this.flag, 1)

        // set callback for click events
        this.level1.inputEnabled = true
        this.level1.events.onInputDown.add((_) => {
            // allow to select only 1 tile 
            if (this.path.length < 2) { // the first one in `this.path` is Dino's position and the second one is the position you want to move to)
                // add tile to the path and update graphics
                this.addToPath(this.marker)
                this.ui.setButtonEnabled("undo", true)
            }
        })
    }

    createFlag() {
        let flag = game.add.sprite(
            (this.xTiles - 1) * this.scaledTileSize + 28,
            getRandomInt(this.yTiles - 1) * this.scaledTileSize - 16,
            'flag');
        flag.scale.set(3.5)
        return flag
    }

    update() {
        if (this.tileMap && this.game.input.activePointer) {
            const hoveredTile = this.tileMap.getTileWorldXY(this.game.input.activePointer.x, this.game.input.activePointer.y, this.scaledTileSize, this.scaledTileSize)
            if (hoveredTile) {
                let highlightPath = this.path.concat([hoveredTile])
                // Dino can walk only by 4 tiles per turn at the moment so: take only the first 4 tiles of the highlighted path
                const dinoHighlightPath = this.createTileByTilePath(highlightPath).slice(0, 4)

                // if no tile has been selected yet
                if (this.path.length < 2) {
                    this.highlightTiles(dinoHighlightPath)
                }

                if (dinoHighlightPath.length > 0) {
                    this.marker.y = dinoHighlightPath.slice(-1)[0].worldY
                    this.marker.x = dinoHighlightPath.slice(-1)[0].worldX
                }
            }
        }
    }

    render() {
        if (this.displayPathHighlight) {
            this.pathGraphics.forEach(pathGraphic => {
                game.debug.geom(pathGraphic, 'rgba(50,50,220,0.5)');
            })

            this.newPathGraphics.forEach(pathGraphic => {
                game.debug.geom(pathGraphic, 'rgba(100,50,220,0.35)')
            })
        }
    }

    /**
     * Add the tile beneath worldPoint to the path member variable 
     * @param {*} worldPoint 
     */
    addToPath(worldPoint) {
        // get the tile at the current pointer position
        let tile = this.getTileFromPosition(worldPoint)
        // add the tile to the path member variable for later usage
        this.path.push(tile)
        // update the highlighted tile graphics to give feedback to the user
        this.pathGraphics.push(
            this.createHighlightTile(tile)
        )
    }

    resetPathGraphics() {
        this.path = []
        // disable the undo button
        this.ui.setButtonEnabled("undo", false)
        this.highlightPath = []
        this.pathGraphics = []
    }

    getTileCoordinateFromPosition(pos) {
        const tile = this.tileMap.getTileWorldXY(pos.x, pos.y, this.scaledTileSize, this.scaledTileSize)
        return { x: tile.x, y: tile.y }
    }

    getTileFromPosition(pos) {
        return this.tileMap.getTileWorldXY(pos.x, pos.y, this.scaledTileSize, this.scaledTileSize)
    }

    /**
     * Create a "tile by tile" path.
     * Usually the `path` argument contains only a couple of tiles (the start point and the end point); this method
     * takes this `path` array and returns a new array containing every tile that an entity needs to cross
     * in order to walk on all the tiles in the `path` argument. 
     * Example:
     * path = [{0,1}, {0,5}]
     * output = [{0,1}, {0,0}, {0,1}, {0,2}, {0,3}, {0,4}, {0,5}]
     * The path is created prioritizing horizontal tiles.
     * 
     * @param {Phaser.Tile[]} path 
     * @returns {Pasher.Tile[]}
     */
    createTileByTilePath(path) {
        if (path.length == 0) {
            console.error(`createTileByTilePath: doesn't accept an empty path array as argument is null`)
            return []
        } else if (path.length == 1) {
            return [path[0]]
        }

        let tileByTilePath = []
        // fill the horizontal tile and then update the vertical tiles
        for (let i = 1; i <= path.length - 1; i++) {
            let fromTile = path[i - 1]
            let toTile = path[i]
            // highlight tiles on the x axis
            if (toTile.x > fromTile.x) {
                for (let i = fromTile.x + 1; i <= toTile.x; i++) {
                    tileByTilePath.push(this.tileMap.getTile(i, fromTile.y))
                }
            }
            else if (toTile.x < fromTile.x) {
                // fill the horizontal tile and then update the vertical tiles
                for (let i = fromTile.x - 1; i >= toTile.x; i--) {
                    tileByTilePath.push(this.tileMap.getTile(i, fromTile.y))
                }
            }
            // highlight tiles on the y axis
            if (toTile.y > fromTile.y) {
                for (let i = fromTile.y + 1; i <= toTile.y; i++) {
                    tileByTilePath.push(this.tileMap.getTile(toTile.x, i))
                }
            }
            else if (toTile.y < fromTile.y) {
                // fill the horizontal tile and then update the vertical tiles
                for (let i = fromTile.y - 1; i >= toTile.y; i--) {
                    tileByTilePath.push(this.tileMap.getTile(toTile.x, i))
                }
            }
        }

        return tileByTilePath
    }

    /**
     *  
     * @param {Phaser.Tile} path 
     */
    highlightTiles(path) {
        this.newPathGraphics = []

        path.forEach(tile => {
            this.newPathGraphics.push(this.createHighlightTile(tile))
        })
    }

    /**
     *
     * @param {Object} pos - Object with x and y coordinates in tile coordinates
     * @param {number} pos.x - x coordinate 
     * @param {number} pos.y - y coordinate
     */
    createHighlightTile(pos) {
        return new Phaser.Rectangle(pos.x * this.scaledTileSize, pos.y * this.scaledTileSize, this.scaledTileSize, this.scaledTileSize)
    }

}

export default Map;