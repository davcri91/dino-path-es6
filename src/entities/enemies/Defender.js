import Phaser from 'phaser'

class Defender extends Phaser.Sprite{
    constructor({game, x, y, asset, tileSize, startDirection}) {
        super(game, x, y, asset)

        this.tileSize = tileSize

        // update offset
        // this.x = x - 8 
        this.scale.set(3)

        // init walk animation
		this.animations.add('walk-down', [0,1,2])
		this.animations.add('walk-up', [3,4,5])
        this.animations.add('walk-right', [6,7,8])
        this.animations.add('walk-left', [9,10,11])
        // this.animations.add('walk-left', [0,1,2])
        this.animFrameRate = 6
        const loop = true
        if (['up', 'down', 'left', 'right'].includes(startDirection)) {
            this.animations.play('walk-' + startDirection, this.animFrameRate, loop)
        } else {
            console.error('Bad value for startDirection: ' + startDirection)
        }

        // // turn variables
        // this.tweens = []
    }

    /**
     * 
     */
    takeTurn() {
        if (this.animations.currentAnim.name === 'walk-down') {
            // move one tile down
            // init tween
            const tween = this.game.add.tween(this).to( { y: this.y + this.tileSize }, 400, "Quart.easeOut")
            // start tween to move by one tile
            tween.start()
            this.animations.play('walk-up', this.animFrameRate, true)
        } else if (this.animations.currentAnim.name === 'walk-up') {
            // init tween
            const tween = this.game.add.tween(this).to( { y: this.y - this.tileSize }, 400, "Quart.easeOut")
            // start tween to move by one tile
            tween.start()
            this.animations.play('walk-down', this.animFrameRate, true)
        } else if (this.animations.currentAnim.name === 'walk-left') {
            // move one tile down
            // init tween
            const tween = this.game.add.tween(this).to( { x: this.x - this.tileSize }, 400, "Quart.easeOut")
            // start tween to move by one tile
            tween.start()
            this.animations.play('walk-right', this.animFrameRate, true)
        } else if (this.animations.currentAnim.name === 'walk-right') {
            // init tween
            const tween = this.game.add.tween(this).to( { x: this.x + this.tileSize }, 400, "Quart.easeOut")
            // start tween to move by one tile
            tween.start()
            this.animations.play('walk-left', this.animFrameRate, true)
        }
    }
}

export default Defender;