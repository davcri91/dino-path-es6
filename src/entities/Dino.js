
class Dino {
    constructor({ game, group }) {
        this.game = game
        this.sprite = game.add.sprite(100, 100, 'dino')
        this.sprite.scale.set(3)
        group.addChildAt(this.sprite, 0)

        // init walk animation
        this.walk = this.sprite.animations.add('walk')
        this.walk.enableUpdate = true;
        const animFrameRate = 6
        const loop = true
        this.sprite.animations.play('walk', animFrameRate, loop)

        // audio
        this.walkSound = this.game.add.audio('step')

        this.hp = 3

        // turn variables
        this.tweens = []
    }

    takeTurn() {
        let curTween = this.tweens.shift()
        if (curTween) {
            curTween.start()
            this.walkSound.play()
        }
    }

    /**
     * Simply move Dino to the destination, using a tween 
     * @param {} destination 
     */
    move(destination) {
        this.game.add.tween(this.sprite).to({
            x: destination.x - 4,
            y: destination.y - 6
        }, 400, Phaser.Easing.Exponential.Out, true);
    }

    /**
     * Create an array of tweens to move Dino to every tile in the path array 
     * @param {Phaser.Tile[]} path 
     */
    createTweensAlognPath(path) {
        this.tweens = []

        path.forEach(tile => {
            this.tweens.push(game.add.tween(this.sprite).to({ x: tile.worldX, y: tile.worldY }, 400, "Quart.easeOut"))
        })
    }

    completedTurn() {
        return this.tweens.length === 0
    }

    getHitbox() {
        const dinoHitbox = this.sprite.getBounds()

        // reduce the size of the hitbox to avoid unwanted collisions
        dinoHitbox.centerY += 28 
        dinoHitbox.bottom -= 46 
        dinoHitbox.centerX += 12 
        dinoHitbox.right -= 30 

        return dinoHitbox
    }

    /**
     * 
     * @param {Integer} damage 
     */
    hit(damage) {
        this.hp -= damage

        if (this.hp <= 0) {
            this.hp = 0
        }
    }

    addHP(amount) {
        this.hp += amount

        if (this.hp > 3) {
            this.hp = 3
        }
    }
}

export default Dino;