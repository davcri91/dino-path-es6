export default {
  gameWidth: 1200,
  gameHeight: 640,
  localStorageName: 'phaseres6webpack',
  webfonts: ['Bangers'],
  transparent: false, // default
  antialias: false,
  textureScaleFactor: 4 // currently used for the tilemap
}
