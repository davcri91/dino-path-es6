class GameUI {
    constructor({game}) {
        // ugly code here, needs refactoring
        this.game = game.game // I know, I know...
        this.gameContext = game
        this.uiGroup = this.game.add.group()

        this.moveBtn = this.game.make.button(0, 400, 'buttons', this.onMoveBtnClick, this, 1, 1)
        this.moveBtn.scale.set(3)

        this.undoBtn = this.game.make.button(0, 480, 'buttons', this.onUndoBtnClick, this, 2, 2)
        this.undoBtn.scale.set(3)

        // button colors 
        this.ENABLED_COLOR = 0xffffff
        this.DISABLED_COLOR = 0x333333

        // set buttons initial state
        this.setButtonEnabled("move", true)
        this.setButtonEnabled("undo", false)
       
        // font styles 
        this.BIG_FONT = '26px Press Start 2P'
        this.NORMAL_FONT = '20px Press Start 2P'

        // HP
        this.hpLabel = new Phaser.Text(this.game, 80, 50, 'HP', {font: this.BIG_FONT})
        this.hp = []
        for (let i = 0; i < 3; i++) {
            this.hp.push(new Phaser.Sprite(this.game, (10 + i*65), 90, 'hearts', 4))
            this.hp[i].scale.set(3)
            this.uiGroup.add(this.hp[i])
        }

        this.uiGroup.add(this.hpLabel) 

        // Score
        this.scoreLabel = new Phaser.Text(this.game, 40, 200, 'SCORE', {font: this.BIG_FONT})
        this.score = new Phaser.Text(this.game, 60, 240, '0', {font: this.NORMAL_FONT})
        this.setScore(String(game.score))
        this.uiGroup.add(this.score)
        this.uiGroup.add(this.scoreLabel) 

        // add buttons to the UI group
        this.uiGroup.add(this.moveBtn);
        this.uiGroup.add(this.undoBtn);

        // register callbacks
        this.callbacks = {
            'move': game.moveDino,
            'undo': game.undo
        }
        // TODO: I can build this dinamically, using the keys of the callbacks obj
        this.supportedEvents = ['move', 'undo']

        // sounds
        this.confirmSound = this.game.add.audio('confirm')

        // position the UI Group
        this.uiGroup.x = this.game.world.width - 250
    }

    onMoveBtnClick() {
        if (this.isButtonEnabled("move")) {
            this.callbacks['move'].call(this.gameContext)
            this.confirmSound.play()
        }
    }

    onUndoBtnClick() {
        if (this.isButtonEnabled("undo")) {
            this.callbacks['undo'].call(this.gameContext)
        }
    }

    isButtonEnabled(buttonStr) {
        if (this.getButtonByString(buttonStr).tint === this.ENABLED_COLOR) {
            return true
        } else {
            return false
        }
    }

    getButtonByString(button) {
        switch (button) {
            case "move":
                return this.moveBtn
            case "undo":
                return this.undoBtn
            default:
                console.error("No button found with string: " + button)
                return undefined
        }
    }

    /**
     * 
     * @param {string} score 
     */
    setScore(score) {
        this.score.text = score
    }
   
    setButtonEnabled(button, enabled) {
        let btn = this.getButtonByString(button)

        if (enabled) {
            btn.tint = this.ENABLED_COLOR
        } else {
            btn.tint = this.DISABLED_COLOR 
        }
    }

    /**
     * 
     * @param {Integer} amount 
     */
    removeHP(amount) {
        for (let i = 0; i < amount; i++) {
            this.uiGroup.remove(this.hp.pop())
        }
    }
    
    /**
     * 
     * @param {Integer} amount 
     */
    addHP(amount) {
        const currentHP = Math.min(3, this.hp.length + amount)

        for (let i = this.hp.length; i < currentHP; i++) {
            this.hp.push(new Phaser.Sprite(this.game, (10 + i*65), 90, 'hearts', 4))
            this.hp[i].scale.set(3)
            this.uiGroup.add(this.hp[i])
        }
    }

    // registerCallback(name, callback)  {
    //     if (this.supportedEvents.includes(name)) {
    //         this.callbacks[name] = callback
    //     } else {
    //         console.error("Cannot register callback for: " + name)
    //     }
    // }
}

export default GameUI;