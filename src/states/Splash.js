import Phaser from 'phaser'
import lang from '../lang'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])
    this.load.setPreloadSprite(this.loaderBar)

    //
    // load your assets
    //
    this.load.image('mushroom', 'assets/images/mushroom2.png')

    // Dino sprite
    this.game.load.spritesheet('dino', 'assets/images/DinoSprites.png', 24, 24, 4)

    // tilemap
    this.load.image('tuxemon-tileset', 'assets/images/tuxemon-tileset.png')
    this.load.image('flag', 'assets/images/flag.png')

    // objects
    this.load.spritesheet('food', 'assets/images/food-4.png', 16, 16, 4)

    // Game UI
    this.load.spritesheet('buttons', 'assets/ui/btns-spritesheet.png', 64, 16, 2)
    this.load.spritesheet('hearts', 'assets/ui/hearts.png', 16, 16, 6)
    this.load.image('start-btn', 'assets/ui/start-btn.png')

    // enemies
    this.load.spritesheet('skeleton', 'assets/images/skeleton.png', 16, 20, 12)

    // audio
    this.load.audio('music', ['assets/audio/381384__uso-sketch__jungle.ogg']);
    this.load.audio('background-audio', ['assets/audio/269570__vonora__cuckoo-the-nightingale-duet.ogg'] )
    this.load.audio('confirm', ['assets/audio/269156__heshl__confirmation-sound.ogg'])
    this.load.audio('step', ['assets/audio/step.ogg'])
    this.load.audio('victory', ['assets/audio/victory.ogg'])
    this.load.audio('pick-item', ['assets/audio/pick-item.ogg'])
    this.load.audio('game-over', ['assets/audio/game-over.wav'])
    this.load.audio('hit', ['assets/audio/hit.wav'])
  }

  create () {
    const bannerText = lang.text('welcome')
    let banner = this.add.text(this.world.centerX, this.game.height - 80, bannerText, {
      font: '40px Bangers',
      fill: '#77BFA3',
      smoothed: false
    })
    banner.padding.set(10, 16)
    banner.anchor.setTo(0.5)

    setTimeout(() => {
      this.state.start('Game')
    }, 100)
  }
}
