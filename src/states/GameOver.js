import Phaser from 'phaser'
import config from '../config';

export default class GameOver extends Phaser.State {
    init(score) {
        this.stage.backgroundColor = '#00000'
        this.score = score
    }

    create() {
        this.BIG_FONT = '26px Press Start 2P'
        this.gameOverLabel = new Phaser.Text(this.game, config.gameWidth/2, config.gameHeight/2, 'Game Over\n\nSCORE: ' + this.score, { font: this.BIG_FONT })
        this.gameOverLabel.anchor.setTo(0.5, 0.5)
        this.gameOverLabel.addColor('#FFFFFF', 0)

        this.scene = this.game.add.group()
        this.undoBtn = this.game.make.button(config.gameWidth/2, config.gameHeight/2 + 200, 'start-btn', this.onStartBtnClick, this, 2, 2)
        this.undoBtn.scale.set(3)
        this.undoBtn.anchor.x = 0.5 
        this.undoBtn.anchor.y = 0.5

        this.scene.add(this.undoBtn)
        this.scene.add(this.gameOverLabel)
    }

    onStartBtnClick() {
        this.state.start('Boot', true, true)
    }
}
