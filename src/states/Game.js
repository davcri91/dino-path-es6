/* globals __DEV__ */
import { Phaser } from 'phaser'
import Dino from '../entities/Dino';
import Map from '../entities/Map';
import GameUI from '../ui/GameUI';
import Defender from '../entities/enemies/Defender';
import { getRandomInt } from '../utils';

class Game extends Phaser.State {
    /**
     * 
     * @param {JSON} params Optional parameters passed by another state 
     */
    init(params) {
        // if we access  
        if (params !== undefined) {
            // use the score from the previous level
            this.score = params.score
        } else {
            this.score = 0
        }
    }

    preload() { }

    create() {
        // enables input
        this.game.input.mouse.capture = true
        this.game.physics.startSystem(Phaser.Physics.ARCADE)

        // init level variables
        this.levelCompleted = false

        // main scene tree
        this.scene = this.game.add.group()

        // initialize game UI
        this.ui = new GameUI({ game: this })

        // load entities
        this.dino = new Dino({ game: this.game, group: this.scene })
        this.tilemap = new Map({ game: this.game, dino: this.dino, group: this.scene, ui: this.ui })

        // place Dino in a random start position 
        this.placeEntityInTile(this.dino, this.tilemap, 0, getRandomInt(this.tilemap.yTiles))
        this.tilemap.addToPath(this.dino.sprite)

        // this.game.physics.enable(this.dino.sprite) // TODO: not sure if this is still used

        // initialize Turn system and timers
        this.timer = game.time.create(false) // false makes the timer not self destructing
        const turnDuration = 500 // ms
        this.timer.loop(turnDuration, this.takeTurn, this)
        this.currentTurn = 0

        // init map objects
        // food
        this.foodObjs = []
        for (let i = 0; i < getRandomInt(5); i++) {
            const randX = getRandomInt(this.tilemap.xTiles - 1)
            const randY = getRandomInt(this.tilemap.yTiles - 1)
            const randTile = this.tilemap.tileMap.getTile(randX, randY)

            const food = game.add.sprite(randTile.worldX, randTile.worldY, 'food', getRandomInt(4))
            // add offset
            food.x += 14
            food.y += 14
            food.scale.set(2)
            this.foodObjs.push(food)
            this.scene.addChildAt(food, 1)
        }

        // init enemies
        this.enemies = []
        const randomEnemiesCount = 6
        for (let i = 0; i < randomEnemiesCount; i++) {
            let randomDirection = 'down'

            switch (getRandomInt(4)) {
                case 0:
                    randomDirection = 'down'
                    break
                case 1:
                    randomDirection = 'up'
                    break
                case 2:
                    randomDirection = 'left'
                    break
                case 3:
                    randomDirection = 'right'
                    break

                default:
                    break
            }

            const randX = getRandomInt(this.tilemap.xTiles - 1)
            const randY = getRandomInt(this.tilemap.yTiles - 1)
            const randTile = this.tilemap.tileMap.getTile(randX, randY)

            let enemy = new Defender({ game: this.game, x: randTile.worldX, y: randTile.worldY, asset: 'skeleton', tileSize: this.tilemap.scaledTileSize, startDirection: randomDirection })
            this.enemies.push(enemy)
            this.scene.addChildAt(this.enemies[i], 1)
        }

        // init audio
        this.music = this.game.add.audio('music')
        this.background = this.game.add.audio('background-audio')
        this.victorySound = this.game.add.audio('victory')
        this.pickItemSound = this.game.add.audio('pick-item')
        this.hit = this.game.add.audio('hit')
        this.music.loop = true
        // this.music.play()
        this.background.loop = true
        this.background.play()

        //
        this.updateState = true 
    }

    update() {
        if (this.updateState) {
            // create the hitbox for the flag
            this.flagHitbox = this.tilemap.flag.getBounds()
            this.flagHitbox.centerY += 24
            this.flagHitbox.bottom -= 26
            this.flagHitbox.centerX += 4
            this.flagHitbox.right -= 26
    
            if (this.dino.hp != 0) {
                // update tilemap marker
                this.tilemap.update()
        
                // check for victory condition
                if (Phaser.Rectangle.intersects(this.flagHitbox, this.dino.getHitbox()) && !this.levelCompleted) {
                    this.levelCompleted = true
                    // add on stop callback
                    this.victorySound.onStop.add(() => {
                        // after the victory fanfare, wait a little bit and then go to the next level
                        setTimeout(() => {
                            this.music.stop()
                            this.background.stop()
                            this.state.start('Game', true, false, {score: this.score})
                        }, 650)
                    })
                    // play victory sound and change to the next level
                    this.victorySound.play()
                }
            } else {
                this.updateState = false                
            }
        } else {
            this.game.camera.fade(0x000000, 1000)
            setTimeout(() => {
                this.state.start('GameOver', true, false, this.score)
            }, 1000)
            this.updateState = true
        }
    }
    
    handlePause() {
        // if the game is over
        if (this.dino.hp == 0) {
            this.game.camera.fade(0x000000, 1000)
            setTimeout(() => {
                this.game.paused = true
                this.state.start('Game')
            }, 1000)
        }
    }

    takeTurn() {
        // if Dino has still moves to do in his turn 
        if (!this.dino.completedTurn()) {
            // execute the moves for entities (player and enemies)
            this.dino.takeTurn()
            this.enemies.forEach(enemy => {
                enemy.takeTurn()
            })
            this.currentTurn++

            // TODO: this works but could be refactored. It should be a callback invoked on enemies turn completed
            setTimeout(() => {
                // check for enemies collisions
                for (let i = this.enemies.length - 1; i >= 0; i--) {
                    if (this.dino.getHitbox().intersects(this.enemies[i].getBounds())) {
                        this.dino.hit(1)
                        this.game.camera.flash(0xffffff, 200)
                        this.ui.removeHP(1)
                        this.hit.play()
                    }
                }
            }, 425) // 425 is just an hardcoded value greater than the duration of the tweens (400ms)
            console.log("TURN " + this.currentTurn)
        } else {
            this.onTurnCompleted()
            this.timer.pause()
            this.tilemap.resetPathGraphics()
            this.tilemap.addToPath(this.dino.sprite)
            this.tilemap.displayPathHighlight = true
        }
        this.onMoveCompleted()
    }

    onMoveCompleted() {
        // check for food objects collisions
        for (let i = this.foodObjs.length - 1; i >= 0; i--) {
            if (!this.foodObjs[i].collided && this.dino.getHitbox().intersects(this.foodObjs[i].getBounds())) {
                this.foodObjs[i].collided = true
                setTimeout(() => {
                    // remove object from array and from group
                    this.scene.remove(this.foodObjs[i])
                    this.foodObjs.splice(i, 1)

                    // update score
                    this.score += 100
                    this.ui.setScore(this.score)

                    // restore 1HP
                    this.dino.addHP(1)
                    this.ui.addHP(1)

                    // play sound
                    this.pickItemSound.play()
                }, 350)
            }
        }

    }

    onTurnCompleted() {
    }

    render() {
        if (__DEV__) {
            // this.game.debug.spriteInfo(this.dino.sprite, 32, 32)
            // this.game.debug.cameraInfo(this.game.camera, 32, 32)
        }

        // render marker and highlight tiles
        this.tilemap.render()

        // --------------------------
        // DEBUG DRAW FOR HITBOXES
        // --------------------------
        // flag hitbox
        // this.game.debug.geom(this.flagHitbox, 'rgba(170,0,220,0.35)')
        // dino hitbox
        // this.game.debug.geom(this.dino.getHitbox(), 'rgba(170,0,220,0.35)')
        // food objects hitbox
        // this.foodObjs.forEach(foodObj=> {
        //     this.game.debug.geom(foodObj.getBounds())
        // });
    }

    /**
     * Called on "MoveBtn" click
     */
    moveDino() {
        this.dino.createTweensAlognPath(
            this.tilemap.createTileByTilePath(this.tilemap.path)
        )

        if (this.timer.paused) {
            this.timer.resume()
        } else {
            // start the turn
            this.timer.start()
        }

        this.tilemap.displayPathHighlight = false

        // this.dino.moveToPath(this.tilemap.createTileByTilePath(this.tilemap.path))
    }

    /**
     * Called on "CancelBtn" click
     */
    undo() {
        if (this.tilemap.path.length > 0) {
            this.tilemap.path.pop()
            this.tilemap.pathGraphics.pop()
            if (this.tilemap.path.length == 1) {
                this.ui.setButtonEnabled("undo", false)
            }
            this.tilemap.highlightTiles(
                this.tilemap.createTileByTilePath(this.tilemap.path)
            )
        }
    }

    /**
     * Move the entity to position, using its "move" method (this method should trigger animations, tweens,...).
     * @param {*} entity 
     * @param {*} tilemap 
     * @param {*} position 
     */
    // moveEntityInTile(entity, tilemap, position) {
    //   var startTile = tilemap.tileMap.getTileWorldXY(position.worldX, position.worldY, this.scaledTileSize, this.scaledTileSize)
    //   // console.log(startTile.x, startTile.y);
    //   if (startTile) {
    //     entity.move({ x: startTile.worldX, y: startTile.worldY })
    //   }
    // }

    placeEntityInTile(entity, tilemap, tileX, tileY) {
        var startTile = tilemap.tileMap.getTile(tileX, tileY)
        entity.sprite.x = startTile.worldX
        entity.sprite.y = startTile.worldY
    }
}

export default Game;