export const centerGameObjects = (objects) => {
  objects.forEach(function (object) {
    object.anchor.setTo(0.5)
  })
}

/**
 * Returns an integer in the range [0, max) (this is mathematical notation and it means that
 * 0 is inclusive and max is exclusive)
 * @param {*} max 
 */
export const getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max))
}