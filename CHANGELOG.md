# Changelog
All notable changes to this project will be documented in this file. This
project adheres to [Semantic Versioning](http://semver.org/).  
Dates are in the following format: [YYYY-MM-DD]

---

# Version 0.1.1 [WIP]
- Changed license from GPL to MIT
- 

### Major changes
- Add sound
- Add background music (if you don't heare any music, it's disabled via code in
  the Game.js file)
- Limit movement to 4 tiles per turn
- Add simple score system when picking food objects
- Add simple HP system

### Fixes

### Documentation

### Game Design changes

1. User can select only one destination tile per turn: this allows for a more intuitive gameplay
and also allows to act more strategically. You can move by 2 tiles just to observe the enemy and
then decide how to move to get to the ending. 

--- 

# Version 0.1.0 [2018-10-06]
Initial version 

Dino can move on a map along a path selected by clicking witht the mouse on the tiles.
If Dino arrives at the final destination, the level will be completed and a new level will be created and displayed.
